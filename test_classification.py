# import demo2
import glob, os, cv2
# from predict_table import MODEL_CLASSIFICATION
from predict_table import MODEL_CLASSIFICATION
import time

model = MODEL_CLASSIFICATION()
# model.get_output_image("data/valid/train")

folder = "data/valid/positive/"
list_all = glob.glob(os.path.join(folder, "*"))
start = 0


for i, link in enumerate(list_all[start:]):
    t1 = time.time()
    # Testing has finished
    print("index: ", start + i, "link: ", link)
    link_img = os.path.join(folder, os.path.basename(link).split(".")[0] + ".jpg")
    img = cv2.imread(link_img)
    # print(img.shape)
    if link_img not in list_all:
        continue
    check = model.get_table_predict(img)
    t2 = time.time()
    print('Time taken was {} seconds'.format(t2 - t1))
    print(check)