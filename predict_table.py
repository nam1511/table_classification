import torch
import torch.nn as nn
from torchvision import models
import cv2
from torchvision import transforms
import numpy as np


class MODEL_CLASSIFICATION(object):
    def __init__(self):
        super().__init__()
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.model = models.mobilenet_v2()
        self.model.classifier[1] = nn.Linear(self.model.last_channel, 2)
        self.model.load_state_dict(torch.load('Model/model_weights.pth', map_location=self.device))
        self.model.eval()
        self.tfms = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])

    def get_table_predict(self, image):
        width = 224
        height = 224
        dim = (width, height)
        image = cv2.resize(image, dim, interpolation=cv2.INTER_AREA)
        image = self.tfms(image)
        image = image.unsqueeze(0)
        y = self.model(image[0].unsqueeze(0))
        pred_probab = nn.Softmax(dim=1)(y)
        y_pred = pred_probab.argmax(1)
        return y_pred.tolist()[0]
